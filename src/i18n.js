import {createI18n} from "vue-i18n";
import {getLocale} from "./utils.js";

import en from "./translations/en.json";
import ro from "./translations/ro.json";
import ru from "./translations/ru.json";

const translations = {
    en, ro, ru
}

const i18n = createI18n({
    legacy: false,
    locale: getLocale(),
    messages: translations
});

export default i18n