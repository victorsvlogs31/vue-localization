import {createRouter, createWebHistory} from "vue-router";
import AnotherPage from "./pages/AnotherPage.vue";
import Dashboard from "./pages/Dashboard.vue";
import MainLayout from "./layouts/MainLayout.vue";

const routes = [
    {
        path: '/',
        redirect: `/en`
    },
    {
        path: '/:locale',
        component: MainLayout,
        children: [
            {path: '', component: Dashboard, name: 'Main'},
            {path: 'another-page', component: AnotherPage, name: 'Another'},
        ]
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router
